﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication8
{
    public class Inime
    {
        public string Nimi;
        public int Hinne = 1;
    }

    public partial class Contact : Page
    {
        List<Inime> Inimesed = new List<Inime>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Inimesed.Count == 0 || Inimesed == null)
            {
                Inimesed = new List<Inime>()
                {
                    new Inime {Nimi = "Henn", Hinne = 5},
                    new Inime {Nimi = "Ants", Hinne = 2},
                    new Inime {Nimi = "Peeter"},
                    new Inime {Nimi = "Joosep"}

                };
            }

            this.GridView1.DataSource = Inimesed.Select(x => new { x.Nimi, x.Hinne }).ToList();
            this.GridView1.DataBind();

        }
    }
}