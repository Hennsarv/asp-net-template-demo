﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebApplication8.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Your contact page.</h3>
    <p>&nbsp;</p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="Nimi" HeaderText="Nimi" />
                <asp:TemplateField HeaderText="Hinne"  >
                    <ItemTemplate>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" SelectedValue='<%# Eval("Hinne") %>'>
                            <asp:ListItem Value="1">Nõrk</asp:ListItem>
                            <asp:ListItem Value="2">Rahuldav</asp:ListItem>
                            <asp:ListItem Value="3">Hea</asp:ListItem>
                            <asp:ListItem Value="4">Väga Hea</asp:ListItem>
                            <asp:ListItem Value="5">Suurepärane</asp:ListItem>
                        </asp:RadioButtonList>
                        
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </p>
    <address>
        One Microsoft Way<br />
        Redmond, WA 98052-6399<br />
        <abbr title="Phone">P:</abbr>
        425.555.0100
    </address>

    <address>
        <strong>Support:</strong>   <a href="mailto:Support@example.com">Suppor</a></address>
    <address>
        <a href="mailto:Support@example.com">t@example.com</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
    </address>
</asp:Content>
